.. balu xml format documentation master file, created by
   sphinx-quickstart on Tue Feb 21 13:22:32 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to balu xml format's documentation!
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
BALU XML data import structure
==============================

::

   # XML example with all types and all possible options
   <?xml version="1.0"?>
   <balu xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <attic>
            <external_id>1000101</external_id>
            <updated_at>13-12-2017 15:55</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>first2 last2</name>
                <tel>123</tel>
                <mob>456</mob>
                <email>test@test.com</email>
                <url>www.test.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <project_id>13</project_id>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <living_area>120</living_area>
                <terrace_area>0</terrace_area>
                <rooms>3</rooms>
                <bathrooms>1</bathrooms>
                <floor>5</floor>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <attic_building_type>living_house</attic_building_type>
                <is_mansard>1</is_mansard>
                <renovation_in_ten_years>Partial</renovation_in_ten_years>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>0</reservoir_in_10min>
                <double_glazed_windows>1</double_glazed_windows>
                <fireplace>1</fireplace>
                <sauna>0</sauna>
                <air_conditioning>1</air_conditioning>
                <furniture_included>1</furniture_included>
                <internet>1</internet>
                <cable_tv>1</cable_tv>
                <air_ventilation>1</air_ventilation>
                <appliances>1</appliances>
                <built_in_kitchen>1</built_in_kitchen>
                <new_doors>1</new_doors>
                <stove>0</stove>
                <alarm>1</alarm>
                <appliance_fridge>1</appliance_fridge>
                <appliance_dishwasher>1</appliance_dishwasher>
                <appliance_dryer>0</appliance_dryer>
                <appliance_oven>1</appliance_oven>
                <appliance_washer>1</appliance_washer>
                <comfort>All</comfort>
                <heating>Central</heating>
                <hot_water>Boiler</hot_water>
                <building_renovation_in_ten_years>1</building_renovation_in_ten_years>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </attic>
        <basement>
            <external_id>1000102</external_id>
            <updated_at>01-02-2017 15:45</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <project_id>13</project_id>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <repairs_year>2012</repairs_year>
                <equipped_basement>1</equipped_basement>
                <building_materials>Wood</building_materials>
                <finishing>Full</finishing>
                <air_conditioning>1</air_conditioning>
                <air_ventilation>1</air_ventilation>
                <heating>Central</heating>
                <renovation_in_ten_years>No</renovation_in_ten_years>
                <double_glazed_windows>1</double_glazed_windows>
                <internet>1</internet>
                <alarm>1</alarm>
                <entrance_from_facade>1</entrance_from_facade>
                <storefronts>1</storefronts>
                <cable_tv>1</cable_tv>
                <new_doors>1</new_doors>
                <comfort>All</comfort>
                <hot_water>Boiler</hot_water>
                <building_type>Dwelling house</building_type>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </basement>
        <building>
            <external_id>1000103</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>1.1</land_area>
                <floors_total>1</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <electrical_capacity>1.1</electrical_capacity>
                <repairs_year>2012</repairs_year>
                <land>Ownership</land>
                <is_leased>1</is_leased>
                <abort_contract>1</abort_contract>
                <reservoir_in_10min>1</reservoir_in_10min>
                <park_in_10min>1</park_in_10min>
                <lease_percent>11</lease_percent>
                <contract_term>11</contract_term>
                <contract_term_length>month</contract_term_length>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <heating>Central</heating>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <air_conditioning>1</air_conditioning>
                <air_ventilation>1</air_ventilation>
                <internet>1</internet>
                <alarm>1</alarm>
                <cable_tv>1</cable_tv>
                <furniture_included>1</furniture_included>
                <gym_in_building>1</gym_in_building>
                <physical_security>1</physical_security>
                <restaurant_in_building>1</restaurant_in_building>
                <equipped_kitchen>1</equipped_kitchen>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
            </costs>
        </building>
        <carservice>
            <external_id>1000104</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <offices_area>120</offices_area>
                <land_area>0</land_area>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <repairs_year>2012</repairs_year>
                <electrical_capacity>1</electrical_capacity>
                <carservice_building_type>Separately standing building/hangar</carservice_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <alarm>1</alarm>
                <telpher>1</telpher>
                <service_elevator>1</service_elevator>
                <truck_road>1</truck_road>
                <secured_area>1</secured_area>
                <air_ventilation>1</air_ventilation>
                <heating>Central</heating>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </carservice>
        <factory>
            <external_id>1000105</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <offices_area>120</offices_area>
                <stocks_area>120</stocks_area>
                <land_area>0</land_area>
                <floor>1</floor>
                <ceilings_height>3.1</ceilings_height>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <building_class>A</building_class>
                <electrical_capacity>1</electrical_capacity>
                <factory_building_type>Industrial/warehouse complex</factory_building_type>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <telpher>1</telpher>
                <branch_line>1</branch_line>
                <truck_road>1</truck_road>
                <ramp>1</ramp>
                <service_elevator>1</service_elevator>
                <secured_area>1</secured_area>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <air_ventilation>1</air_ventilation>
                <alarm>1</alarm>
                <heating>Central</heating>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </factory>
        <farm>
            <external_id>1000106</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <total_area>150</total_area>
                <land_area>1.1</land_area>
                <area_land_type>ha</area_land_type>
                <buildings_area>1.1</buildings_area>
                <as_forest>1</as_forest>
                <as_farmland>1</as_farmland>
                <deforestation>1</deforestation>
                <reservoir>1</reservoir>
                <land_in_processing>1</land_in_processing>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>1</reservoir_in_10min>
                <electricity>1</electricity>
                <gas>1</gas>
                <water_availability>Possibilty to connect municipal communications</water_availability>
                <sewage_availability>Possibility to connect municipal communications</sewage_availability>
                <road_condition>No access roads</road_condition>
            </params>
            <costs>
                <territory_maintenance_cost>1.1</territory_maintenance_cost>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
            </costs>
        </farm>
        <flat>
            <external_id>1000107</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <project_id>13</project_id>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <terrace_area>0</terrace_area>
                <rooms>3</rooms>
                <bathrooms>1</bathrooms>
                <floor>5</floor>
                <floors_total>12</floors_total>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>0</reservoir_in_10min>
                <double_glazed_windows>1</double_glazed_windows>
                <fireplace>1</fireplace>
                <sauna>0</sauna>
                <air_conditioning>1</air_conditioning>
                <furniture_included>1</furniture_included>
                <internet>1</internet>
                <cable_tv>1</cable_tv>
                <air_ventilation>1</air_ventilation>
                <appliances>1</appliances>
                <built_in_kitchen>1</built_in_kitchen>
                <new_doors>1</new_doors>
                <stove>0</stove>
                <alarm>1</alarm>
                <appliance_fridge>1</appliance_fridge>
                <appliance_dishwasher>1</appliance_dishwasher>
                <appliance_dryer>0</appliance_dryer>
                <appliance_oven>1</appliance_oven>
                <appliance_washer>1</appliance_washer>
                <comfort>All</comfort>
                <heating>Central</heating>
                <hot_water>Boiler</hot_water>
                <building_renovation_in_ten_years>1</building_renovation_in_ten_years>
                <new_roof>1</new_roof>
                <new_windows>1</new_windows>
                <renovated_facade>1</renovated_facade>
                <renovated_staircase>1</renovated_staircase>
                <changed_risers>1</changed_risers>
                <building_materials>Wood</building_materials>
                <pets_allowed>1</pets_allowed>
                <smoking_allowed>1</smoking_allowed>
                <residence_declaration>1</residence_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <maintainer>консьерж сервис</maintainer>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <land_under_rent>1</land_under_rent>
                <land_rent>1.1</land_rent>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </flat>
        <gym>
            <external_id>1000108</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <gym_area>1.1</gym_area>
                <land_area>1.1</land_area>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <ceilings_height>3.1</ceilings_height>
                <bathrooms>1</bathrooms>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <repairs_year>2012</repairs_year>
                <gym_building_type>Dwelling house</gym_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <air_conditioning>1</air_conditioning>
                <air_ventilation>1</air_ventilation>
                <equipped_kitchen>1</equipped_kitchen>
                <pool>1</pool>
                <furniture_included>1</furniture_included>
                <heating>Central</heating>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <internet>1</internet>
                <sauna>0</sauna>
                <equipped_dressing_rooms>1</equipped_dressing_rooms>
                <showcases>1</showcases>
                <alarm>1</alarm>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </gym>
        <hangar>
            <external_id>1000109</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <offices_area>120</offices_area>
                <land_area>0</land_area>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <electrical_capacity>1</electrical_capacity>
                <width>1.1</width>
                <height>1.1</height>
                <entrance_height>1.1</entrance_height>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <alarm>1</alarm>
                <truck_road>1</truck_road>
                <ramp>1</ramp>
                <land>Ownership</land>
                <telpher>1</telpher>
                <branch_line>1</branch_line>
                <secured_area>1</secured_area>
                <service_elevator>1</service_elevator>
                <air_ventilation>1</air_ventilation>
                <heating>Central</heating>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </hangar>
        <home>
            <external_id>1000110</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>120</land_area>
                <terrace_area>0</terrace_area>
                <rooms>3</rooms>
                <bathrooms>1</bathrooms>
                <floors_total>12</floors_total>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <repairs_year>2012</repairs_year>
                <type_id>1</type_id>
                <area_location>Private settlement</area_location>
                <garage_type>Separately standing garage</garage_type>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>0</reservoir_in_10min>
                <home_construction_material>Wood</home_construction_material>
                <finishing>Full</finishing>
                <renovation_in_ten_years>Partial</renovation_in_ten_years>
                <double_glazed_windows>1</double_glazed_windows>
                <fireplace>1</fireplace>
                <sauna>0</sauna>
                <air_conditioning>1</air_conditioning>
                <furniture_included>1</furniture_included>
                <internet>1</internet>
                <cable_tv>1</cable_tv>
                <air_ventilation>1</air_ventilation>
                <appliances>1</appliances>
                <built_in_kitchen>1</built_in_kitchen>
                <new_doors>1</new_doors>
                <stove>0</stove>
                <alarm>1</alarm>
                <appliance_fridge>1</appliance_fridge>
                <appliance_dishwasher>1</appliance_dishwasher>
                <appliance_dryer>0</appliance_dryer>
                <appliance_oven>1</appliance_oven>
                <appliance_washer>1</appliance_washer>
                <comfort>All</comfort>
                <heating>Central</heating>
                <hot_water>Boiler</hot_water>
                <sewage>Municipal</sewage>
                <water_supply>Municipal</water_supply>
                <pets_allowed>1</pets_allowed>
                <smoking_allowed>1</smoking_allowed>
                <residence_declaration>1</residence_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <territory_maintenance_cost>1.1</territory_maintenance_cost>
                <maintainer>консьерж сервис</maintainer>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </home>
        <hotel>
            <external_id>1000111</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>0</land_area>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <persons>1</persons>
                <rooms>1</rooms>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>1</reservoir_in_10min>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <air_conditioning>1</air_conditioning>
                <alarm>1</alarm>
                <equipped_kitchen>1</equipped_kitchen>
                <restaurant_in_building>1</restaurant_in_building>
                <air_ventilation>1</air_ventilation>
                <internet>1</internet>
                <gym_in_building>1</gym_in_building>
                <physical_security>1</physical_security>
                <furniture_included>1</furniture_included>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <heating>Central</heating>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </hotel>
        <land>
            <external_id>1000112</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <total_area>150</total_area>
                <area_land_type>ha</area_land_type>
                <type_id>1</type_id>
                <plot_count>11</plot_count>
                <area_location>Private settlement</area_location>
                <foundation>1</foundation>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>1</reservoir_in_10min>
                <electricity>1</electricity>
                <gas>1</gas>
                <construction_project>1</construction_project>
                <water_availability>Possibilty to connect municipal communications</water_availability>
                <sewage_availability>Possibility to connect municipal communications</sewage_availability>
                <road_condition>No access roads</road_condition>
                <plots>1</plots>
                <area_communications>1</area_communications>
                <roads_between_areas>1</roads_between_areas>
                <sector_road_condition>Asphalt</sector_road_condition>
                <land_in_processing>1</land_in_processing>
                <deforestation>1</deforestation>
                <taxation>1</taxation>
            </params>
            <costs>
                <territory_maintenance_cost>1.1</territory_maintenance_cost>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <property_tax_required>1</property_tax_required>
            </costs>
        </land>
        <parking>
            <external_id>1000113</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <total_area>150</total_area>
                <parking_spaces>2</parking_spaces>
                <width>1.1</width>
                <height>1.1</height>
                <entrance_height>1.1</entrance_height>
                <type_id>1</type_id>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </parking>
        <office>
            <external_id>1000114</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>120</land_area>
                <terrace_area>0</terrace_area>
                <rooms>3</rooms>
                <bathrooms>1</bathrooms>
                <floor>1</floor>
                <floors_total>12</floors_total>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <type_id>1</type_id>
                <building_class>A</building_class>
                <finishing>Full</finishing>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <cable_tv>1</cable_tv>
                <air_ventilation>1</air_ventilation>
                <alarm>1</alarm>
                <hot_water>Boiler</hot_water>
                <sewage>Municipal</sewage>
                <water_supply>Municipal</water_supply>
                <equipped_kitchen>1</equipped_kitchen>
                <gym_in_building>1</gym_in_building>
                <office_equipment>1</office_equipment>
                <shower>1</shower>
                <physical_security>1</physical_security>
                <restaurant_in_building>1</restaurant_in_building>
                <office_building_type>Office centre</office_building_type>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <space_type>Separate office</space_type>
                <open_space>1</open_space>
                <building_materials>Wood</building_materials>
                <legal_entity_declaration>1</legal_entity_declaration>
                <heating>Central</heating>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </office>
        <restaurant>
            <external_id>1000115</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <terrace_area>120</terrace_area>
                <premises_area>120</premises_area>
                <land_area>0</land_area>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <entrances>1</entrances>
                <bathrooms>1</bathrooms>
                <persons>1</persons>
                <restaurant_building_type>Dwelling house</restaurant_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <air_conditioning>1</air_conditioning>
                <alarm>1</alarm>
                <equipped_kitchen>1</equipped_kitchen>
                <restaurant_bar_counter>1</restaurant_bar_counter>
                <air_ventilation>1</air_ventilation>
                <internet>1</internet>
                <restaurant_furniture>1</restaurant_furniture>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <heating>Central</heating>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </restaurant>
        <salon>
            <external_id>1000116</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <premises_area>120</premises_area>
                <land_area>0</land_area>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <entrances>1</entrances>
                <bathrooms>1</bathrooms>
                <salon_building_type>Dwelling house</salon_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <air_conditioning>1</air_conditioning>
                <alarm>1</alarm>
                <air_ventilation>1</air_ventilation>
                <internet>1</internet>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <heating>Central</heating>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </salon>
        <sauna>
            <external_id>1000117</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>0</land_area>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <bathrooms>1</bathrooms>
                <persons>1</persons>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <repairs_year>2012</repairs_year>
                <sauna_building_type>Dwelling house</sauna_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <park_in_10min>1</park_in_10min>
                <reservoir_in_10min>0</reservoir_in_10min>
                <air_conditioning>1</air_conditioning>
                <air_ventilation>1</air_ventilation>
                <equipped_kitchen>1</equipped_kitchen>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <heating>Central</heating>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <sleeping_spaces>1</sleeping_spaces>
                <pool>1</pool>
                <reservoir>1</reservoir>
                <furniture_included>1</furniture_included>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </sauna>
        <shop>
            <external_id>1000118</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <shopping_area>120</shopping_area>
                <land_area>0</land_area>
                <entrances>999</entrances>
                <bathrooms>1</bathrooms>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <air_ventilation>1</air_ventilation>
                <alarm>1</alarm>
                <heating>Central</heating>
                <shop_building_type>Shop in the dwelling house</shop_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </shop>
        <stock>
            <external_id>1000119</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <offices_area>120</offices_area>
                <land_area>0</land_area>
                <floor>1</floor>
                <ceilings_height>3.1</ceilings_height>
                <floors_total>2</floors_total>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <repairs_year>2012</repairs_year>
                <building_class>A</building_class>
                <electrical_capacity>1</electrical_capacity>
                <stock_building_type>Premises in the warehouse complex</stock_building_type>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <telpher>1</telpher>
                <branch_line>1</branch_line>
                <truck_road>1</truck_road>
                <ramp>1</ramp>
                <service_elevator>1</service_elevator>
                <secured_area>1</secured_area>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <air_conditioning>1</air_conditioning>
                <internet>1</internet>
                <air_ventilation>1</air_ventilation>
                <alarm>1</alarm>
                <heating>Central</heating>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </stock>
        <workshop>
            <external_id>1000120</external_id>
            <updated_at>01-02-2017 15:43</updated_at>
            <action>FOR_SALE</action>
            <owner>
                <name>Janis Ivanovs</name>
                <tel>+371 6666666</tel>
                <mob>+371 2222222</mob>
                <email>Janis@janis.com</email>
                <url>www.realestatecompany.com</url>
            </owner>
            <status>ACTIVE</status>
            <private>0</private>
            <location>
                <city>Riga</city>
                <address>Brivibas iela 73</address>
                <address_lat>56.9496000000</address_lat>
                <address_lng>24.1052000000</address_lng>
            </location>
            <price>250000</price>
            <price_currency>EUR</price_currency>
            <description>
                <lv>garš sludinajuma apraksts...</lv>
                <ru>длинное описание объявления...</ru>
                <en>long advert description…</en>
            </description>
            <images>
                <image>http://torentor.net/uploads/posts/2016-12/1482755092_passazhiry-2016.jpg</image>
                <image>https://static.cityreal.lv/typo3temp/GB/ddf3eccb96.jpg</image>
                <image>http://torentor.net/uploads/posts/2016-12/1482947756_kredo-ubiycy-2016.jpg</image>
            </images>
            <rent_costs>
                <pay_period>day</pay_period>
                <deposit_amount>11</deposit_amount>
                <contract_term_from>11</contract_term_from>
                <contract_term_from_type>day</contract_term_from_type>
                <contract_term_till>11</contract_term_till>
                <contract_term_till_type>day</contract_term_till_type>
            </rent_costs>
            <params>
                <foundation_id>1</foundation_id>
                <total_area>150</total_area>
                <land_area>1.1</land_area>
                <floor>1</floor>
                <floors_total>2</floors_total>
                <ceilings_height>3.1</ceilings_height>
                <parking>1</parking>
                <parking_spaces>2</parking_spaces>
                <elevator>1</elevator>
                <electrical_capacity>1</electrical_capacity>
                <repairs_year>2012</repairs_year>
                <workshop_building_type>Dwelling house</workshop_building_type>
                <building_materials>Wood</building_materials>
                <land>Ownership</land>
                <finishing>Full</finishing>
                <renovation_in_five_years>Was not done</renovation_in_five_years>
                <internet>1</internet>
                <air_conditioning>1</air_conditioning>
                <air_ventilation>1</air_ventilation>
                <alarm>1</alarm>
                <heating>Central</heating>
                <water_supply>Municipal</water_supply>
                <sewage>Municipal</sewage>
                <legal_entity_declaration>1</legal_entity_declaration>
            </params>
            <costs>
                <average_summer_bill>1.1</average_summer_bill>
                <average_winter_bill>1.1</average_winter_bill>
                <maintenance_fee>1.1</maintenance_fee>
                <property_tax>1.1</property_tax>
                <cadastral_value>1.1</cadastral_value>
                <average_summer_bill_required>1</average_summer_bill_required>
                <average_winter_bill_required>1</average_winter_bill_required>
                <property_tax_required>1</property_tax_required>
            </costs>
        </workshop>
   </balu>

::

   # Possible types
   attic
   basement
   building
   carservice
   factory
   farm
   flat
   gym
   hangar
   home
   hotel
   land
   office
   parking
   restaurant
   salon
   shop
   stock
   workshop

::

   # Common elements for all types

   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | Element                                   | Type                | Description                                                            |
   +===========================================+=====================+========================================================================+
   | <rent_costs> / <contract_term_from_type>  | String(choice)      | Link for advert images. Can be many images. Elements one after another |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <external_id>                             | Integer             | Advert ID in broker DB                                                 |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <updated_at>                              | Datetime            |                                                                        |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <action>                                  | String(choice)      | Advert action(FOR_SALE/FOR_RENT)                                       |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <owner> / <name>                          | String              | Name, Surname of advert owner                                          |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <owner> / <tel>                           | String              | Owner contact phone                                                    |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <owner> / <mob>                           | String              | Owner contact mobile phone                                             |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <owner> / <email>                         | String              | Owner contact e-mail                                                   |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <owner> / <url>                           | String              | Owner home page url                                                    |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <status>                                  | String(choice)      | Advert status(ACTIVE,PENDING,DELETED)                                  |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <private>                                 | Boolean             | Advert visibility for public                                           |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <location> / <city>                       | String              | Advert property city                                                   |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <location> / <address>                    | String              | Advert property address                                                |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <location> / <address_lat>                | Decimal             | Gps coordinate for lat                                                 |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <location> / <address_lng>                | Decimal             | Gps coordinate for lng                                                 |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <price>                                   | Decimal             | Advert property price                                                  |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <price_currency>                          | Decimal             | Advert property price currency symbol(EUR)                             |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <description> / <lv>                      | String(1000)        | Advert description in Latvian                                          |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <description> / <ru>                      | String(1000)        | Advert description in Russian                                          |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <description> / <en>                      | String(1000)        | Advert description in English                                          |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <images> / <image>                        | String              | Link for advert images. Can be many images. Elements one after another |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <pay_period>               | String(choice)      | Rent pay period (day, month)                                           |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <deposit_amount>           | Decimal             |                                                                        |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <contract_term_from>       | Integer             |                                                                        |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <contract_term_from_type>  | String(choice)      | Possible values (day, month, year)                                     |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <contract_term_till>       | Integer             |                                                                        |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+
   | <rent_costs> / <contract_term_till_type>  | String(choice)      | Possible values (day, month, year)                                     |
   +-------------------------------------------+---------------------+------------------------------------------------------------------------+

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
